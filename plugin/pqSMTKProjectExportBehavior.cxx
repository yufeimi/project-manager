//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectExportBehavior.h"

#include "config/Registry.h"
#include "config/Simulation.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Model.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/operators/ImportPythonOperation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"
#include "smtk/view/Configuration.h"

// Paraview client
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFileInfo>
#include <QIcon>
#include <QMessageBox>
#include <QScrollArea>
#include <QSharedPointer>
#include <QString>
#include <QTextStream>
#include <QVBoxLayout>
#include <QVariant>
#include <QtGlobal>

#include "nlohmann/json.hpp"

#include "boost/filesystem.hpp"

#include <algorithm> // std::replace
#include <cassert>

using json = nlohmann::json;

#define ALERT_ICON_PATH ":/icons/attribute/errorAlert.png"

//-----------------------------------------------------------------------------
pqProjectExportReaction::pqProjectExportReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectExportReaction::exportProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Get project and simulation-specific config
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto project = projectManager->getCurrentProject();

  std::string simCode = project->simulationCode();
  smtk::shared_ptr<config::Simulation> config = config::Registry::getConfig(simCode.c_str());

  // Unregister any previously-loaded export op
  auto opManager = wrapper->smtkOperationManager();
  if (!m_exportOperationUniqueName.empty())
  {
    opManager->unregisterOperation(m_exportOperationUniqueName);
    m_exportOperationUniqueName = "";
  }

  // Try getting path to export operator in workflows folder
  std::string opFilename;
  std::string opLocation;
  if (config && config->exportOpFilename(opFilename))
  {
    vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
    if (proxy)
    {
      vtkSMProperty* workflowProp = proxy->GetProperty("WorkflowsFolder");
      auto workflowStringProp = vtkSMStringVectorProperty::SafeDownCast(workflowProp);
      if (workflowStringProp)
      {
        std::string workflowsFolder = workflowStringProp->GetElement(0);
        boost::filesystem::path workflowsPath(workflowsFolder);
        boost::filesystem::path opPath = workflowsPath / opFilename;
        if (boost::filesystem::exists(opPath))
        {
          opLocation = opPath.string();
        } // if
      }   // if
    }     // if (proxy)
  }       // if (config)

  smtk::operation::OperationPtr exportOp;
  auto& logger = smtk::io::Logger::instance();
  if (!opLocation.empty())
  {
    // Trying loading export operator

    smtk::operation::OperationPtr importPythonOp =
      opManager->create<smtk::operation::ImportPythonOperation>();
    if (!importPythonOp)
    {
      smtkErrorMacro(logger, "Could not create \"import python operation\"");
    }
    else
    {
      importPythonOp->parameters()->findFile("filename")->setValue(opLocation);
      smtk::operation::Operation::Result result;
      try
      {
        result = importPythonOp->operate();
        int outcome = result->findInt("outcome")->value();
        if (outcome == static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
        {
          // On success, the ImportPythonOperation creates a "unique_name" value.
          // Use that string to create the export operator, and save that string to (later) release
          // the export operator.
          m_exportOperationUniqueName = result->findString("unique_name")->value();
          exportOp = opManager->create(m_exportOperationUniqueName);
        }
        else
        {
          smtkErrorMacro(smtk::io::Logger::instance(),
            "\"import python operation\" operation failed, outcome " << outcome);
        }
      } // try
      catch (std::exception& e)
      {
        smtkErrorMacro(smtk::io::Logger::instance(), e.what());
      }
    } // else
  }   // if (opLocation)

  if (exportOp == nullptr)
  {
    // Used default logic if needed
    bool reset = true;
    exportOp = projectManager->getExportOperator(logger, reset);
  }

  if (exportOp == nullptr)
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed Getting Export Operator"),
      tr(logger.convertToString().c_str()));
    return;
  }

  // Check for jobs panel object, which migh have newtSessionId;
  QVariant newtSessionId;
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "pqCumulusJobsPanel missing pqApplicationCore";
  }
  else
  {
    QObject* jobsPanelObject = pqCore->manager("jobs_panel");
    // qDebug() << "jobsPanelObject" << jobsPanelObject;
    if (jobsPanelObject != nullptr)
    {
      newtSessionId = jobsPanelObject->property("newt_sessionid");
#ifndef NDEBUG
      if (newtSessionId.isValid())
      {
        qDebug() << "NEWT Session id:" << newtSessionId.toString();
      }
#endif
    }
  } // else

  // Apply any simulation-specific initialization
  bool ok = true;
  if (config != nullptr)
  {
    if (newtSessionId.isValid())
    {
      config->JsonProps["newt_sessionid"] = newtSessionId.toString().toStdString();
    }
    ok = config->preExport(project, exportOp);
  }
  else
  {
    qDebug() << "No config instance for" << QString::fromStdString(simCode);
  }

  if (!ok)
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "An error occurred initializing the export operator."
       << " Check the output messages for more info.";
    QMessageBox::warning(
      pqCoreUtilities::mainWidget(), tr("Failed Configuring Export Operator"), msg);
    return;
  }

  // Construct a modal dialog for the operation.
  QDialog* exportDialog = new QDialog(pqCoreUtilities::mainWidget());
  exportDialog->setObjectName("SimulationExportDialog");
  exportDialog->setWindowTitle("Simulation Export Dialog");

  // Make dialog contents scrollable.
  QScrollArea* scroll = new QScrollArea(exportDialog);
  QWidget* viewport = new QWidget(exportDialog);
  scroll->setWidget(viewport);
  scroll->setWidgetResizable(true);
  QVBoxLayout* viewLayout = new QVBoxLayout(viewport);
  viewport->setLayout(viewLayout);

  // Create operation view for the export operation.
  auto uiManager = new smtk::extension::qtUIManager(
    exportOp, wrapper->smtkResourceManager(), wrapper->smtkViewManager());
  smtk::view::ConfigurationPtr vconfig = uiManager->findOrCreateOperationView();

  // Check for toplevel view (besides the operation view)
  smtk::view::ConfigurationPtr topView;
  auto topViewList = exportOp->specification()->findTopLevelViews();
  for (auto view : topViewList)
  {
    if (view->type() != "Operation")
    {
      topView = view;
      break;
    }
  }

  if (topView != nullptr)
  {
    // Copy topView filters to operation view
    std::vector<std::string> booleanAtts = { "FilterByAdvanceLevel", "FilterByCategory" };
    for (auto booleanAtt : booleanAtts)
    {
      if (topView->details().attributeAsBool(booleanAtt))
      {
        vconfig->details().setAttribute(booleanAtt, "true");
      }
    } // for (booleanAtt)
  }   // if (topView)

  smtk::extension::qtOperationView* opView =
    dynamic_cast<smtk::extension::qtOperationView*>(uiManager->setSMTKView(vconfig, viewport));
  opView->showInfoButton(false);

  QVBoxLayout* layout = new QVBoxLayout(exportDialog);
  exportDialog->setLayout(layout);
  exportDialog->layout()->addWidget(scroll);
  exportDialog->resize(640, 480);
  exportDialog->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // Alert the user if the operation fails. Close the dialog if the operation
  // succeeds.
  smtk::operation::Operation::Result result;
  connect(opView, &smtk::extension::qtOperationView::operationExecuted,
    [=](const smtk::operation::Operation::Result& result) {
      if (result->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
      {
        QMessageBox msgBox(pqCoreUtilities::mainWidget());
        msgBox.setStandardButtons(QMessageBox::Ok);
        // Create a spacer so it doesn't look weird
        QSpacerItem* horizontalSpacer =
          new QSpacerItem(300, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
        msgBox.setText("Export failed. See the \"Output Messages\" view for more details.");
        QGridLayout* layout = (QGridLayout*)msgBox.layout();
        layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
        msgBox.exec();

        // Once the user has accepted that their export failed, they are
        // free to try again without changing any options.
        opView->onModifiedParameters();
      }
      else
      {
        qInfo() << "Export operation completed";
        exportDialog->done(QDialog::Accepted);

        // Check for warnings
        // Some scripts use a string item
        std::size_t numWarnings = 0;
        auto warningsItem = result->findString("warnings");
        if (warningsItem && warningsItem->numberOfValues() > 0)
        {
          numWarnings = warningsItem->numberOfValues();
          for (std::size_t i = 0; i < numWarnings; ++i)
          {
            qWarning() << QString::fromStdString(warningsItem->value(i));
          }
        }

        // Alternatively check log item
        if (warningsItem == nullptr)
        {
          auto logItem = result->findString("log");
          std::string logString = logItem->value(0);
          if (!logString.empty())
          {
            auto jsonLog = json::parse(logString);
            assert(jsonLog.is_array());
            for (json::iterator it = jsonLog.begin(); it != jsonLog.end(); ++it)
            {
              auto jsRecord = *it;
              assert(jsRecord.is_object());
              if (jsRecord["severity"] >= static_cast<int>(smtk::io::Logger::WARNING))
              {
                numWarnings++;
                std::string content = jsRecord["message"];
                // Escape any quote signs (formats better)
                std::replace(content.begin(), content.end(), '"', '\"');
                qWarning("%zu. %s", numWarnings, content.c_str());
              } // if (>= warning)
            }   // for (it)
          }     // if (logString)
        }       // if (warningsItem)
        qDebug() << "Number of warnings:" << numWarnings;

        if (numWarnings > 0)
        {
          QWidget* parentWidget = pqCoreUtilities::mainWidget();
          QString text;
          QTextStream qs(&text);
          qs << "WARNING: The generated file is INCOMPLETE or INVALID."
             << " You can find more details in the Output Messages view."
             << " You will generally need to correct all invalid input fields"
             << " in the Attribute Editor in order to generate a valid " << simCode.c_str()
             << " input file."
             << " Look in the Attribute Editor panel for red \"alert\" icons"
             << " and input fields with red background."
             << "\n\nNumber of errors: " << numWarnings;
          QMessageBox msgBox(
            QMessageBox::NoIcon, "Export Warnings", text, QMessageBox::NoButton, parentWidget);
          msgBox.setIconPixmap(QIcon(ALERT_ICON_PATH).pixmap(32, 32));
          msgBox.exec();
        }
      } // else
    });

  // Launch the modal dialog and wait for the operation to succeed.
  exportDialog->exec();
  delete uiManager;
  exportDialog->deleteLater();
} // exportProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectExportBehavior* g_instance = nullptr;

pqSMTKProjectExportBehavior::pqSMTKProjectExportBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectExportBehavior* pqSMTKProjectExportBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectExportBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectExportBehavior::~pqSMTKProjectExportBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
