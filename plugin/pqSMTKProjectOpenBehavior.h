//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectOpenBehavior_h
#define pqSMTKProjectOpenBehavior_h

#include "pqReaction.h"

#include <QObject>

/// A reaction for writing a resource manager's state to disk.
class pqProjectOpenReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectOpenReaction(QAction* parent);

  void openProject();

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override { this->openProject(); }

private:
  Q_DISABLE_COPY(pqProjectOpenReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqSMTKProjectOpenBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqSMTKProjectOpenBehavior* instance(QObject* parent = nullptr);
  ~pqSMTKProjectOpenBehavior() override;

protected:
  pqSMTKProjectOpenBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectOpenBehavior);
};

#endif
