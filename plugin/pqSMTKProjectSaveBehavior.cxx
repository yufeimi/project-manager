//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectSaveBehavior.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"

// // Client side
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

//-----------------------------------------------------------------------------
pqProjectSaveReaction::pqProjectSaveReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectSaveReaction::saveProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Get project manager and call saveProject()
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto& logger = smtk::io::Logger::instance();
  if (!projectManager->saveProject(logger))
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed To Save Project"),
      tr(logger.convertToString().c_str()));
    return;
  }

  qInfo() << "Saved project";
} // saveProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectSaveBehavior* g_instance = nullptr;

pqSMTKProjectSaveBehavior::pqSMTKProjectSaveBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectSaveBehavior* pqSMTKProjectSaveBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectSaveBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectSaveBehavior::~pqSMTKProjectSaveBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
