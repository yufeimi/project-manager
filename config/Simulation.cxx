//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "Simulation.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/model/operators/AssignColors.h"
#include "smtk/project/Project.h"

#include "boost/filesystem.hpp"

namespace config
{

bool Simulation::exportOpFilename(std::string& filename) const
{
  // Should be overridden by subclasses
  return false;
}

bool Simulation::setModelFile(smtk::attribute::FileItemPtr fileItem,
  smtk::project::ProjectPtr project, const std::string& modelIdentifier,
  smtk::io::Logger& logger) const
{
  // Check all vars
  if ((fileItem == nullptr) || (project == nullptr))
  {
    return false;
  }

  auto modelResource = project->findResource<smtk::model::Resource>(modelIdentifier);
  if (modelResource == nullptr)
  {
    return false;
  }

  fileItem->setIsEnabled(true);
  std::string importLocation = project->importLocation(modelResource);
  if (!importLocation.empty())
  {
    // Check project directory for the same filename
    boost::filesystem::path projectDir(project->directory());
    boost::filesystem::path importPath(importLocation);
    auto projectMeshPath = projectDir / importPath.filename();
    if (boost::filesystem::exists(projectMeshPath))
    {
      fileItem->setValue(0, projectMeshPath.string());
    }
    else if (boost::filesystem::exists(importPath))
    {
      fileItem->setValue(0, importPath.string());
    }
    else
    {
      std::stringstream ss;
      ss << "Unable to find mesh file at \"" << importPath.string() << "\"";
      smtkWarningMacro(logger, ss.str());
    }
  } // if (importLocation)

  return true;
}

bool Simulation::assignColors(smtk::model::ResourcePtr modelResource,
  smtk::model::BitFlags entityType, const std::vector<std::string>& palette)
{
  auto colorOp = smtk::model::AssignColors::create();

  // Associate model entities
  auto uuidSet = modelResource->entitiesMatchingFlags(entityType, true);
  for (auto uuid : uuidSet)
  {
    auto modelEnt = modelResource->findEntity(uuid);
    colorOp->parameters()->associate(modelEnt);
  }

  smtk::attribute::StringItemPtr colorsItem = colorOp->parameters()->findString("colors");
  colorsItem->setIsEnabled(true);
  colorsItem->setNumberOfValues(palette.size());
  colorsItem->setValues(palette.begin(), palette.end());

  auto result = colorOp->operate();
  return true;
}

} // namespace config
