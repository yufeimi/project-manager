# Set source files
set(sources
  ACE3P.cxx
  Registry.cxx
  Simulation.cxx
  Truchas.cxx
)

# Set header files
set(headers
  ACE3P.h
  Registry.h
  Simulation.h
  Truchas.h
)

# Add the library
add_library(projectConfig ${sources})

target_link_libraries(projectConfig
  LINK_PUBLIC
    smtkCore
    Boost::filesystem
  )

target_include_directories(projectConfig
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
  )

generate_export_header(projectConfig EXPORT_FILE_NAME Exports.h)

smtk_get_kit_name(name dir_prefix)

# Install the header files
install(
  FILES
    ${headers}
    ${CMAKE_CURRENT_BINARY_DIR}/Exports.h
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

# Install the library and exports
install(
  TARGETS projectConfig
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix})

if (ENABLE_TESTING)
  add_subdirectory(testing)
endif()
