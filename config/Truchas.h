//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_Truchas_h
#define project_config_Truchas_h

#include "Simulation.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/model/EntityTypeBits.h"

#include <string>

namespace config
{

/// Applies Truchas-specific logic to projects.
class Truchas : public Simulation
{
public:
  /// Get the filename of the export operator
  bool exportOpFilename(std::string& filename) const override;

  /// Apply changes after project created
  bool postCreate(smtk::project::ProjectPtr project) const override;

  /// Apply changes after model imported into project
  bool postImportModel(smtk::project::ProjectPtr project, const std::string& role) const override;

  /// Apply changes before project export dialog is displayed
  bool preExport(
    smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const override;

  /// Assign simcode-specific properties to project resources
  void tagResources(smtk::project::ProjectPtr project) const override;

protected:
  void assignAltModelResource(smtk::project::ProjectPtr project, bool okToCreateAtt = false) const;
  bool isInductionHeatingEnabled(smtk::attribute::ResourcePtr& attResource) const;
  bool preExportV1(smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const;
  bool preExportV2(smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const;
  void renameModelEntities(smtk::model::ResourcePtr resource, smtk::model::EntityTypeBits entType,
    const std::string& prefix) const;
};

} // namespace config

#endif
