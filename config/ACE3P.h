//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_ACE3P_h
#define project_config_ACE3P_h

#include "Simulation.h"

#include "smtk/PublicPointerDefs.h"

#include "nlohmann/json.hpp"

#include <string>

namespace config
{

/// Applies ACE3P-specific logic to projects.
class ACE3P : public Simulation
{
public:
  /// Get the filename of the export operator
  bool exportOpFilename(std::string& filename) const override;

  /// Apply changes after project created
  bool postCreate(smtk::project::ProjectPtr project) const override;

  /// Apply changes before project export dialog is displayed
  bool preExport(
    smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const override;

protected:
  void configNERSC(smtk::attribute::AttributePtr params) const;
};

} // namespace config

#endif
