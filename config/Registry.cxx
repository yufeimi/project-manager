//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "config/Registry.h"

#include "config/ACE3P.h"
#include "config/Truchas.h"

namespace config
{

smtk::shared_ptr<Simulation> Registry::getConfig(const std::string& simulationCode)
{
  if (simulationCode == "ace3p")
  {
    return std::make_shared<ACE3P>();
  }

  if (simulationCode == "truchas")
  {
    return std::make_shared<Truchas>();
  }

  // (else)
  return nullptr;
}

} // namespace config
